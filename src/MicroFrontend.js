import React from 'react';
import {HistoryContext} from "./history-context";

class MicroFrontend extends React.Component {

  componentDidMount() {
    const { name, host, document } = this.props;
    const scriptId = `micro-frontend-script-${name}`;

    if (document.getElementById(scriptId)) {
      this.renderMicroFrontend();
      // console.log("already created!!")
      return;
    }

    fetch(`${host}/asset-manifest.json`)
      .then(res => res.json())
      .then(manifest => {
        const script = document.createElement('script');
        script.id = scriptId;
        script.crossOrigin = '';
        script.src = `${host}${manifest['main.js']}`;
        script.onload = this.renderMicroFrontend;
        document.head.appendChild(script);
        // console.log("newly created!!");
      });
  }

  componentWillUnmount() {
    const { name, window } = this.props;

    window[`unmount${name}`](`${name}-container`);
  }

  renderMicroFrontend = () => {
    const { name, window } = this.props;
    if (name === "Yard") {

      let {history} = this.props;
      // console.log(JSON.stringify(history, null, 2));
      window[`render${name}`](`${name}-container`, history);
    } else {

      let history =  this.context;
      // console.log(JSON.stringify(history, null, 2));
      window[`render${name}`](`${name}-container`, history);
    }
  };

  render() {
    return <main id={`${this.props.name}-container`} />;
  }
}

MicroFrontend.defaultProps = {
  document,
  window,
};

MicroFrontend.contextType = HistoryContext;

export default MicroFrontend;
