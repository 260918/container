import React from 'react';
import { createBrowserHistory } from 'history';
import {HistoryContext} from "./history-context";
import SupplyRequest from "./SupplyRequest";


const defaultHistory = createBrowserHistory();

const App = () => (

    <HistoryContext.Provider value={defaultHistory}>
        <SupplyRequest />
    </HistoryContext.Provider>
);

export default App;
