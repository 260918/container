import React from "react";
import { createBrowserHistory } from 'history';

const defaultHistory = createBrowserHistory();
export const HistoryContext = React.createContext(
    defaultHistory
)