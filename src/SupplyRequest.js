import React from "react";
import AppHeader from "./AppHeader";
import MicroFrontend from "./MicroFrontend";
import {Route, Router, Switch} from "react-router-dom";
import About from "./About";
import {HistoryContext} from "./history-context";

const {
    REACT_APP_ITEMLIST_HOST: itemListHost,
    REACT_APP_YARD_HOST: yardHost,
} = process.env;

const Yard = ({history}) => {

    // console.log(JSON.stringify(history, null, 2));
    // console.log(JSON.stringify(match, null, 2));
    // console.log(JSON.stringify(location, null, 2));
    return <MicroFrontend history={history} host={yardHost} name="Yard"/>
}

export default class SupplyRequest extends React.Component{

    render() {

        let history = this.context;

        return (
            <Router history={history}>
                <React.Fragment>
                    <AppHeader/>
                    <div>
                        <MicroFrontend host={itemListHost} name="ItemList"/>
                    </div>
                    <div>
                        <Switch>
                            <Route exact path="/item/:id" component={Yard}/>
                            <Route exact path="/about" component={About}/>
                        </Switch>
                    </div>
                </React.Fragment>
            </Router>
        )
    }
}

SupplyRequest.contextType = HistoryContext;